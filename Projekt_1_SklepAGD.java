package com;

import java.util.Scanner;

public class Projekt_1_SklepAGD {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w naszym sklepie! Podaj proszę cenę towaru, który chcesz u nas kupić.");
        double cena = scanner.nextInt();
        System.out.println("Na ile rat życzyłbyś sobie rozbić spłatę tego sprzętu?");
        double raty = scanner.nextInt();
        double WysokoscRaty0 = cena / raty;
        if (cena >= 100 && cena <= 10_000 && raty >=6 && raty <=12 ) {
            System.out.println("Wysokość raty wynosi: " + WysokoscRaty0 * 1.025);
        } else if (cena >= 100 && cena <= 10_000 && raty >= 13 && raty <= 24){
            System.out.println("Wysokość raty wynosi: " + WysokoscRaty0 * 1.05);
        }else if (cena >=100 && cena <= 10_000 && raty >= 25 && raty <= 48){
            System.out.println("Wysokość raty wynosi: " + WysokoscRaty0 * 1.1);
        }else {
            System.out.println("Płatność ratalna możliwa jest przy zakupach od 100 do 10 000 zł " +
                    "oraz przy ilości rat od 6 do 48");
        }

    }
}
